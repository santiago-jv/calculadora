class Calculator: 
    
    def readNumbers(self):
        self.firstNumber = float(input("\t\tIngresa el primer número: "))
        self.secondNumber = float(input("\t\tIngresa el segundo número: "))

    def sumar(self):
        self.readNumbers()
        suma = self.firstNumber + self.secondNumber
        print(f"\n\t\tResultado: {self.firstNumber} + {self.secondNumber} = {suma}")

    def functionResta(self):
        self.readNumbers()
        resta = self.firstNumber - self.secondNumber
        print(resta)
    
    def multiplicacion(self):
        self.readNumbers()
        print(self.firstNumber * self.secondNumber)
        
    def division(self):
        self.readNumbers()
        try:
            print(self.firstNumber / self.secondNumber)
        except ZeroDivisionError as e:
            print(e)



option = None
calculator = Calculator()
while option != 5:
    print("\t\t\t   Menú principal")
    print("\t\t\t Calculadora básica\n")
    print("\t\t1. Suma\n\t\t2. Resta\n\t\t3. Multiplicación\n\t\t4. División\n\t\t5. Salir")
    option = int(input("\n\t\tEscoja una opción: "))


    if option == 1:
        #Ejecute la función suma
        calculator.sumar()
        
    elif option == 2:
        calculator.functionResta()
				#Ejecute la función resta
				
    elif option == 3:
        calculator.multiplicacion()
				
    elif option == 4:
        calculator.division()
        #Ejecute la función División
    
    elif option != 5:
        print("\t\tOpción inválida.Intenta de nuevo.")

    input("Oprima Enter para continuar")        
    print("\n")
